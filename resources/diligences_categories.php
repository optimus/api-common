<?php
function get()
{
	global $optimus_connection;
	$results = $optimus_connection->query("SELECT * FROM optimus.diligences_categories")->fetchAll(PDO::FETCH_OBJ);
	return array("code" => 200, "data" => $results);
}
?>