<?php
function get()
{
	global $optimus_connection;
	$results = $optimus_connection->query("SELECT * FROM optimus.dossiers_domaines")->fetchAll(PDO::FETCH_OBJ);
	return array("code" => 200, "data" => $results);
}
?>