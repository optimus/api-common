<?php
function get()
{
	global $optimus_connection, $input;

	$input->nom = $input->path[2];
	validate('nom', $input->path[2], 'string', true);

	if (!$input->nom)
		return array("code" => 400, "message" => "le paramètre 'nom' n'a pas été renseigné");
	
	$communes = $optimus_connection->prepare("SELECT DISTINCT commune_insee, code_postal, nom, latitude, longitude FROM optimus.communes WHERE nom LIKE :nom");
	$communes->bindValue(':nom', $input->nom.'%', PDO::PARAM_STR);
	
	if($communes->execute())
		if ($communes->rowCount() == 0)
			return array("code" => 404, "message" => "Aucune commune ne correspond au nom recherché");
		else
			return array("code" => 200, "data" => $communes->fetchAll(PDO::FETCH_OBJ));
	else
		return array("code" => 400, "message" => $communes->errorInfo()[2]);
}
?>