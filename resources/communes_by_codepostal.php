<?php
function get()
{
	global $optimus_connection, $input;

	$input->code_postal = $input->path[2];
	validate('code_postal', $input->path[2], 'integer', true);

	if (!$input->code_postal)
		return array("code" => 400, "message" => "le paramètre 'codepostal' n'a pas été renseigné");
	
	if (strlen($input->code_postal) < 2)
		return array("code" => 400, "message" => "le paramètre 'codepostal' doit contenir au moins les 2 premiers chiffres");
	
	if (strlen($input->code_postal) > 5)
		return array("code" => 400, "message" => "le paramètre 'codepostal' doit contenir au maximum 5 chiffres");
	
	$communes = $optimus_connection->prepare("SELECT DISTINCT commune_insee, code_postal, nom, latitude, longitude FROM optimus.communes WHERE code_postal LIKE :code_postal");
	$communes->bindValue(':code_postal', $input->code_postal.'%', PDO::PARAM_STR);

	if($communes->execute())
		if ($communes->rowCount() == 0)
			return array("code" => 404, "message" => "Aucune commune ne correspond au code postal recherché");
		else
			return array("code" => 200, "data" => $communes->fetchAll(PDO::FETCH_OBJ));
	else
		return array("code" => 400, "message" => $communes->errorInfo()[2]);
}
?>