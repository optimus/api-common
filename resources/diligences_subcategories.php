<?php
function get()
{
	global $optimus_connection, $input;

	$input->categorie = $input->path[2];
	validate('categorie', $input->path[2], 'integer', false);
	
	if (isset($input->categorie))
	{
		$subcategories = $optimus_connection->prepare("SELECT id, value FROM optimus.diligences_subcategories WHERE categorie = :categorie");
		$subcategories->bindParam(':categorie', $input->categorie, PDO::PARAM_INT);
		
		if($subcategories->execute())
			if ($subcategories->rowCount() == 0)
				return array("code" => 404, "message" => "Aucune sous categorie n'existe pour la categorie renseignée");
			else
				return array("code" => 200, "data" => $subcategories->fetchAll(PDO::FETCH_OBJ));
		else
			return array("code" => 400, "message" => $subcategories->errorInfo()[2]);
	}
	else
		return array("code" => 200, "data" => $optimus_connection->query("SELECT id, categorie, value FROM optimus.diligences_subcategories")->fetchAll(PDO::FETCH_OBJ));
}
?>