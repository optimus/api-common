<?php
function get()
{
	global $input;

	$input->siret = $input->path[2];
	validate('siret', $input->path[2], 'integer', true);

	if (!$input->siret)
		return array("code" => 400, "message" => "le paramètre 'siret' n'a pas été renseigné");
	
	if (strlen($input->siret) != 9)
		return array("code" => 400, "message" => "le paramètre 'siret' doit contenir 9 chiffres");
	
	$bodacc = array();
	$nb_pages = bodacc_count($input->siret);

	for ($i=1; $i<=$nb_pages; $i++)
		$bodacc = array_merge($bodacc, bodacc($input->siret, $i));

	if (sizeof($bodacc) == 0)
		return array("code" => 404, "message" => "Aucune donnée n'est disponible pour ce numéro siret");
	else
		return array("code" => 200, "data" => $bodacc);
}

function bodacc($siret, $page)
{
	$postdata = http_build_query(array('registre' => $siret, 'page' => $page));
	$opts = array('http' =>array('method'  => 'POST','header'  => 'Content-type: application/x-www-form-urlencoded','content' => $postdata));
	$context  = stream_context_create($opts);
	$result = file_get_contents('https://www.bodacc.fr/annonce/liste', false, $context);
	$result = substr($result,strpos($result,'<tr class="pair">'));
	$result = substr($result,0,strpos($result,'</table>'));

	if ($result != "")
	{
		$DOM = new DOMDocument;
		$DOM->loadHTML($result);
		$trs = $DOM->getElementsByTagName('tr');
		foreach ($trs as $tr)
		{
			$dd = $tr->getElementsByTagName('dd');
			$a = $tr->getElementsByTagName('a');

			$annonces[] = array(
			'date'  => $tr->childNodes->item(0)->nodeValue,
			'type'  => $dd->item(2)->nodeValue,
			'depot' => $dd->item(4)->nodeValue,
			'href'  => $a->item(0)->getAttribute('href'));
		}
	}
	return $annonces;
}

function bodacc_count($siret)
{
	$postdata = http_build_query(array('registre' => $siret, 'page' => 1));
	$opts = array('http' =>array('method'  => 'POST','header'  => 'Content-type: application/x-www-form-urlencoded','content' => $postdata));
	$context  = stream_context_create($opts);
	$result = file_get_contents('https://www.bodacc.fr/annonce/liste', false, $context);

	$nb_annonces = substr($result,strpos($result,"Nombre d'annonces trouv&eacute;es : ")+36,16);
	$nb_annonces = substr($nb_annonces,0,strpos($nb_annonces,'</h3>'));
	$nb_pages = intval(($nb_annonces-1) / 10) + 1;

	return $nb_pages;
}
?>