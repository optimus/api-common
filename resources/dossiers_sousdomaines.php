<?php
function get()
{
	global $optimus_connection, $input;

	$input->domaine = $input->path[2];
	validate('domaine', $input->path[2], 'integer', false);
	
	if (isset($input->domaine))
	{
		$sousdomaines = $optimus_connection->prepare("SELECT id, value FROM optimus.dossiers_sousdomaines WHERE domaine = :domaine");
		$sousdomaines->bindParam(':domaine', $input->domaine, PDO::PARAM_INT);
		
		if($sousdomaines->execute())
			if ($sousdomaines->rowCount() == 0)
				return array("code" => 404, "message" => "Aucun sous-domaine n'existe pour le domaine renseigné");
			else
				return array("code" => 200, "data" => $sousdomaines->fetchAll(PDO::FETCH_OBJ));
		else
			return array("code" => 400, "message" => $sousdomaines->errorInfo()[2]);
	}
	else
		return array("code" => 200, "message" => $optimus_connection->query("SELECT id, domaine, value FROM optimus.dossiers_sousdomaines")->fetchAll(PDO::FETCH_OBJ));
}
?>