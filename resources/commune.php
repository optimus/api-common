<?php
function get()
{
	global $optimus_connection, $input;

	$input->commune_insee = $input->path[2];
	validate('commune_insee', $input->path[2], 'integer', true);

	if (!$input->commune_insee)
		return array("code" => 400, "message" => "le paramètre 'commune_insee' n'a pas été renseigné");
	
	if (strlen($input->commune_insee) != 5)
		return array("code" => 400, "message" => "le paramètre 'commune_insee' doit contenir 5 chiffres");
	
	$commune = $optimus_connection->prepare("SELECT commune_insee, code_postal, nom, latitude, longitude FROM optimus.communes WHERE commune_insee = :commune_insee");
	$commune->bindParam(':commune_insee', $input->commune_insee, PDO::PARAM_INT);
	
	if($commune->execute())
		if ($commune->rowCount() == 0)
			return array("code" => 404, "message" => "Aucune commune ne correspond au code INSEE recherché");
		else
			return array("code" => 200, "data" => $commune->fetch(PDO::FETCH_OBJ));
	else
		return array("code" => 400, "message" => $commune->errorInfo()[2]);
}
?>