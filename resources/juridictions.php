<?php
function get()
{
	global $optimus_connection, $input;

	$input->commune_insee = $input->path[2];
	validate('commune_insee', $input->path[2], 'integer', true);

	if (!$input->commune_insee)
		return array("code" => 400, "message" => "le paramètre 'commune_insee' n'a pas été renseigné");
	
	if (strlen($input->commune_insee) != 5)
		return array("code" => 400, "message" => "le paramètre 'commune_insee' doit contenir 5 chiffres");
	
	$juridictions = $optimus_connection->prepare("SELECT * FROM optimus.communes_to_juridictions WHERE commune_insee = :commune_insee");
	$juridictions->bindParam(':commune_insee', $input->commune_insee, PDO::PARAM_INT);
	

	if($juridictions->execute())
		if ($juridictions->rowCount() == 0)
			return array("code" => 404, "message" => "Aucune juridiction n'a été trouvée pour ce code INSEE");
		else
			foreach($juridictions->fetchAll(PDO::FETCH_OBJ) as $juridiction)
				$juridictions_details[] = $optimus_connection->query("SELECT nom, addresse1, addresse2, commune_insee, code_postal, commune, telephone, fax, courriel FROM optimus.juridictions WHERE id = '" . $juridiction->juridiction_id  . "'")->fetch(PDO::FETCH_OBJ);
	else
		return array("code" => 400, "message" => $juridictions->errorInfo()[2]);
	
	return array("code" => 200, "data" => $juridictions_details);
}
?>