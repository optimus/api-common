<?php
header("Access-Control-Allow-Origin: " . $input->origin);
header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, DELETE, OPTIONS");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Headers: Content-Type, Accept, Authorization, X-Requested-With");
header("Access-Control-Max-Age: 1");
header("Content-Type: application/json");

if ($_SERVER['REQUEST_METHOD'] == "OPTIONS")
	die(http_response_code(200));

foreach ($input->path as $key => $value)
	if (!preg_match("/^[a-zA-Z0-9_\-]+$/", $value))
		$result = array("code" => 400, "message" => "Chemin invalide");

if (!isset($result))
{
	if (@$_GET['data'] && @$_GET['data']!='{}' && ($_SERVER['REQUEST_METHOD']=='GET' OR $_SERVER['REQUEST_METHOD']=='DELETE'))
		$input->body = json_decode(urldecode($_GET['data']));
	else if (file_get_contents("php://input") && file_get_contents("php://input")!='{}')
		$input->body = json_decode(file_get_contents("php://input"));

 	if (json_last_error() !== JSON_ERROR_NONE)
		$result = array("code" => 400, "message" => "JSON invalide");
}

if (!isset($result))
	if (file_exists('api_optimus-common/resources/' . $input->path[1] . '.php'))
		include_once 'api_optimus-common/resources/' . $input->path[1] . '.php';
	else
		$result = array("code" => 400, "message" => "Resource inconnue");
else
	$result = array("code" => 400, "message" => "Ressource inconnue");

include_once 'config.php';
include_once 'connect.php';

//include_once 'api_allspark/JWT.php';
include_once 'api_allspark/functions.php';

include_once 'api_common/config.php';
$optimus_connection = new PDO("mysql:host=" . $host, $username, $password);

if (!isset($result))
	$result = function_exists(strtolower($_SERVER['REQUEST_METHOD'])) ? strtolower($_SERVER['REQUEST_METHOD'])() : array("code" => 501, "message" => 'Méthode non implémentée');

http_response_code($result['code']);
echo json_encode($result);
?>